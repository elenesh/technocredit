<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package technocredit
 */


?>

<aside id="secondary-contact" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'Sidebar-contact' ); ?>
</aside><!-- #secondary -->

