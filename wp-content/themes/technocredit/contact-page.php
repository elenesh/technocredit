
<?php
/*
Template Name: contact Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package technocredit
 */

 ?>

	
	<?php
	if(is_front_page())
{
    get_header('front');
}
else
{
    get_header();
}
?>


			
		<div class="icons page">
			  <div class="container">
			      <div class="col-md-12">
			      <div class="imagesicon">
				       <div class="icons-pic">
			  	
						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0005_logotype111.png" />
		               </div>

		               <div class="icons-pic">
		              
						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0003_phone391.png" />
		               </div>

		              <div class="icons-pic">

						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0002_Vector-Smart-Object.png" />
		              </div>

		              <div class="icons-pic">  
		                <img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0017_car122.png" />
                      
                    </div>
                   </div> 
                </div>
             </div>
		</div>
		
		
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		

	
<?php
get_sidebar();
get_footer();
		
		
	



