
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package technocredit
 */

get_header(); ?>



	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">
			
			<div class="icons page">
			  <div class="container">
			      <div class="col-md-12">
			      <div class="imagesicon">
				       <div class="icons-pic">
			  	
						<a href="#"><img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0005_logotype111.png" />
		               </a>
		               </div>

		               <div class="icons-pic">
		              
						<a href="#"><img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0003_phone391.png" />
		               </a></div>

		              <div class="icons-pic">

						<a href="#"><img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0002_Vector-Smart-Object.png" />
		              </a>
		              </div>

		              <div class="icons-pic">  
		                <a href"#"><img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0017_car122.png" />
                        </a>
                    </div>
                   </div> 
                </div>
             </div>
		</div>
		<div class="container">
		<div class="about">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
	</main>

<?php
get_sidebar();
get_footer();
?>


