
<?php
/*
Template Name: about-us Page
*
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package technocredit
 */

 ?>

	
	<?php
	if(is_front_page())
{
    get_header('front');
}
else
{
    get_header();
}
?>

</div>

<?php  
			$args = array("post_type"=>'technoslider');
			$loop = new WP_Query($args);
		?>
	<!--*********************slider************************-->
		<div class="slider loading">
			<!-- background for carousel menu -->
		

		<div style="z-index:200" class="slider-nav">
			<div class="prev"><img class="navigator-left" src="http://technocredit.hostwise.ge/wp-content/uploads/2016/02/left.png"></div>
			<div class="next"><img class="navigator-right" src="http://technocredit.hostwise.ge/wp-content/uploads/2016/02/right.png"></div>
		</div>
			<div id="slider1_container"  class = "bxslider" data-options='pager: false,  nextSelector: ".next", prevSelector: ".prev",   prevText: "", nextText: ""' data-call="bxslider">
			<?php while ($loop->have_posts()) :$loop->the_post(); 
				
			?>

			    <!-- Slides Container -->
			   
			         <div class="gz-single-slide" style=" background-size:cover; background-repeat: no-repeat; background-image: url('<?php echo get_post_meta($loop->post->ID,'wpcf-slide-background-image', true) ?>');">
				  		

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $posts_page_id ), 'single-post-thumbnail' );
  $image = $image[0]; ?>

<div id="techno-slider" class="slider-content" style="background: url('<?php echo $image; ?>'); background-size:cover!important; background-position: 50% 0!important;">
  

<div class="slider-image">
					  <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ) ?>" title="<?php echo get_the_title(); ?>" />		
					  		</div>


					  		



					  		<div class="slider-description">
					  			<h3 class="slider-title"><?php echo get_the_title(); ?></h3>
                                                          
                                                            
					  			<p class="slider-text"><?php echo get_the_excerpt(); ?></p>
					  			<h4 class="slider-price"><?php echo $price; ?></h4>
                                                         
                                                         
					  		</div>
					  		<div style="clear:both; float:none !important;"></div>
				  			
				  		</div>
				  </div>


			
			<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>



		<!--*********************slider[END]************************-->


		




	

<?php
  if(is_front_page()) {
    get_sidebar('home');
}
else {
	get_sidebar();
}

?>


		<?php get_sidebar( 'bottom-sidebar' ); ?>

		<div class="icons home-p">
			<div class="site-content">
		
			<div class="iccons">	
				<div class="icons-img">

			  	
						<img src="http://technocredit.ge/wp-content/uploads/2016/02/apple.svg" />
				</div>
				<div class="iconp">
				
						<p>iphone ტარების უფლებით</p>

				</div>

				

           
                <div class="icons-img">
						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0003_phone391.png" />
				</div>
				<div class="iconp">		
						<p>ლომბარდის პორტირება 0%-ში</p>
				</div>
          
                <div class="icons-img">
						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0002_Vector-Smart-Object.png" />
				</div>
				<div class="iconp">		
						<p>iphone ტარების უფლებით</p>
				</div>
                 <div class="icons-img">
						<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0017_car122.png" />
				</div>
				<div class="iconp">		
						<p>iphone ტარების უფლებით</p>
                </div>

                </div>
             
			</div>
		</div>

		
			
			<?php get_footer(); 

		
		
	



