<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package technocredit
 */


?>

<aside id="secondary-home" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-home' ); ?>
</aside><!-- #secondary -->

