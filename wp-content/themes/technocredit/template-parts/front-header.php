<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package technocredit
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'technocredit' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->
		
		<div class="container">
		<div class="col-md-8">
    <div class='site-logo'>
        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
    </div>
    </div>
    </div>


		<nav id="site-navigation" class=" main-navigation" role="navigation">
				<nav class="navbar navbar-inverse">
                   <div class="container-fluid">
                      <div class="container menu">
				          <div class="col-md-8">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'technocredit' ); ?></button>
							
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			              </div>

			              	<div class="declaration">
			              		<button class="decl"><a href="">განცხადების შევსება</a></button>
			              	</div>
			       
			        </div>
			      </nav>
			  </nav>
			
		</nav><!-- #site-navigation -->
		<div class="icons">
			<div class="container">
				<div class="icons-img">
			  	
				<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0005_logotype111.png" />
           
              
				<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0003_phone391.png" />
          
              
				<img src="http://technocredit.ge/wp-content/uploads/2016/01/fullstory_0002_Vector-Smart-Object.png" />
                </div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
	
