<?php  
			$args = array("post_type"=>'product', "meta_key"=>"wpcf-show-at-homepage", "meta_value"=>1);
			$loop = new WP_Query($args);
		?>
	<!--*********************slider************************-->
		<div class="slider loading">
			<!-- background for carousel menu -->
		

		<div style="z-index:200" class="slider-nav">
			<div class="prev"><img class="navigator-left" src="http://gamezone.ge/wp-gz_contect/themes/gamezone/images/slider_ctrl_icon_left.png"></div>
			<div class="next"><img class="navigator-right" src="http://gamezone.ge/wp-gz_contect/themes/gamezone/images/slider_ctrl_icon_right.png"></div>
		</div>
			<div id="slider1_container"  class = "bxslider" data-options='pager: false,  nextSelector: ".next", prevSelector: ".prev",   prevText: "", nextText: ""' data-call="bxslider">
			<?php while ($loop->have_posts()) :$loop->the_post(); 
				$product = new WC_Product( $loop->post->ID );
				$price = $product->get_price_html();
			?>

			    <!-- Slides Container -->
			   
			        <div class="gz-single-slide" style="background-color: #000; background-size:cover; background-repeat: no-repeat; background-image: url('<?php echo get_post_meta($loop->post->ID,'wpcf-slide-background-image', true) ?>');">
				  		<div id="gamezone-slider" class="slider-content">
					  		<div class="slider-image">
					  			<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ) ?>" title="<?php echo get_the_title(); ?>">
					  		</div>
					  		<div class="slider-description">
					  			<h3 class="slider-title"><?php echo get_the_title(); ?></h3>
					  			<p class="slider-text"><?php echo get_the_excerpt(); ?></p>
					  			<h4 class="slider-price"><?php echo $price; ?></h4>
					  		</div>
					  		<div style="clear:both; float:none !important;"></div>
				  			
				  		</div>
				  </div>


			
			<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>

		<!--*********************slider[END]************************-->