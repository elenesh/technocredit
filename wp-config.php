<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'techno');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pwze+}snX0S`Tv(Jr7fGl</Ru0j,VXD|ByO:g`!ULtgIdJg?QT; c?-Z3[d2Q*=>');
define('SECURE_AUTH_KEY',  'No&QI_ee,YC(das6y0eYg&ccM9D+u-e/jcCSd9=#hg+<;+{37X-SwU(0s,,f{ iT');
define('LOGGED_IN_KEY',    '|f}t9NbWh=0N51X,aN3a[k1E=idu>D{;=lh ?~n31DWW11rlJ%m2R+!-_`b51.SE');
define('NONCE_KEY',        'ln/vC@I]TI1f|63yy&BeXlwk2--7!a!-?0w)vEa`s&h&NgXX.lpL#t07tcA3TF(z');
define('AUTH_SALT',        '4}.?7!PLj-{LHg<+yJ9dqjehObs7klX?Qoi<2s@+1fqD/6,^;e#i:IYF]zsnYh8(');
define('SECURE_AUTH_SALT', 'U0?#^%;-X.5?WQoKa!O(QoUk=HL=K>Ap~5jyefKZn#a*-?x@f@P|LH?<m+XJWs]-');
define('LOGGED_IN_SALT',   'N:=wG+U#(4:W}=:K,x03MK%/L-2|d/ aiGS-Z1M.?}FO0Gk/h!DNA)oI IQ-T-|4');
define('NONCE_SALT',       '<^#oLFT EvMFo-<]jL5caQ-)c$&Hd#Qgs[2Tzi)v9NBW8)ht=G|*4I/-~3qnloq<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
